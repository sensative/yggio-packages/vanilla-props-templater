'use strict';

const assert = require('assert');
const {generateValidator} = require('../src');

describe('basics.test', () => {

  describe('comprehensive template (for basics)', () => {

    const template = {

      a0: 'string',
      a1: 'stringOrNil',
      a2: 'stringOrNull',

      b0: 'bool',
      b1: 'boolOrNil',
      b2: 'boolOrNull',

      c0: 'number',
      c1: 'numberOrNil',
      c2: 'numberOrNull',

      d0: 'object',
      d1: 'objectOrNil',
      d2: 'objectOrNull',

      f0: 'array',
      f1: 'arrayOrNil',
      f2: 'arrayOrNull',

    };
    const validateProps = generateValidator(template);
    console.log({validateProps});

    it('all filled in', () => {
      const value = {
        a0: 'arghh',
        a1: 'arghh',
        a2: 'arghh',

        b0: true,
        b1: true,
        b2: true,

        c0: 23,
        c1: 23,
        c2: 23,

        d0: {},
        d1: {},
        d2: {},

        f0: [],
        f1: [],
        f2: [],
      };
      assert.doesNotThrow(() => validateProps(value));
    });

    it('nulls where possible', () => {
      const value = {
        a0: 'arghh',
        a1: null,
        a2: null,

        b0: true,
        b1: null,
        b2: null,

        c0: 23,
        c1: null,
        c2: null,

        d0: {},
        d1: null,
        d2: null,

        f0: [],
        f1: null,
        f2: null,
      };
      assert.doesNotThrow(() => validateProps(value));
    });

    it('undefined where possible', () => {
      const value = {
        a0: 'arghh',
        a1: undefined,
        a2: null,

        b0: true,
        b1: undefined,
        b2: null,

        c0: 23,
        c1: undefined,
        c2: null,

        d0: {},
        d1: undefined,
        d2: null,

        f0: [],
        f1: undefined,
        f2: null,
      };
      assert.doesNotThrow(() => validateProps(value));
    });

  });

});
