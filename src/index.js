'use strict';

const _ = require('lodash');
const {PropTypes, validateProps} = require('vanilla-prop-types');


// TODO: but these can wait:

// oneOf: isOneOf,
// arrayOf: isArrayOf,
// objectOf: isObjectOf,
// shape: isShape,


const SERIALIZEABLE_PROP_TYPES = {
  string: 'string',
  stringOrNil: 'stringOrNil',
  stringOrNull: 'stringOrNull',
  bool: 'bool',
  boolOrNil: 'boolOrNil',
  boolOrNull: 'boolOrNull',
  number: 'number',
  numberOrNil: 'numberOrNil',
  numberOrNull: 'numberOrNull',

  object: 'object',
  objectOrNil: 'objectOrNil',
  objectOrNull: 'objectOrNull',

  array: 'array',
  arrayOrNil: 'arrayOrNil',
  arrayOrNull: 'arrayOrNull',
};

const validateTemplate = validateProps(
  PropTypes.objectOf(PropTypes.oneOf(_.values(SERIALIZEABLE_PROP_TYPES)).isRequired).isRequired,
);

const generateValidator = template => {
  validateTemplate(template);
  const propTypesInput = _.mapValues(template, templateValue => {
    switch (templateValue) {

      case SERIALIZEABLE_PROP_TYPES.string: {
        return PropTypes.string.isRequired;
      }
      case SERIALIZEABLE_PROP_TYPES.stringOrNil: {
        return PropTypes.string;
      }
      case SERIALIZEABLE_PROP_TYPES.stringOrNull: {
        return PropTypes.string.isRequiredOrNull;
      }

      case SERIALIZEABLE_PROP_TYPES.bool: {
        return PropTypes.bool.isRequired;
      }
      case SERIALIZEABLE_PROP_TYPES.boolOrNil: {
        return PropTypes.bool;
      }
      case SERIALIZEABLE_PROP_TYPES.boolOrNull: {
        return PropTypes.bool.isRequiredOrNull;
      }

      case SERIALIZEABLE_PROP_TYPES.number: {
        return PropTypes.number.isRequired;
      }
      case SERIALIZEABLE_PROP_TYPES.numberOrNil: {
        return PropTypes.number;
      }
      case SERIALIZEABLE_PROP_TYPES.numberOrNull: {
        return PropTypes.number.isRequiredOrNull;
      }

      case SERIALIZEABLE_PROP_TYPES.object: {
        return PropTypes.plainObject.isRequired;
      }
      case SERIALIZEABLE_PROP_TYPES.objectOrNil: {
        return PropTypes.plainObject;
      }
      case SERIALIZEABLE_PROP_TYPES.objectOrNull: {
        return PropTypes.plainObject.isRequiredOrNull;
      }

      case SERIALIZEABLE_PROP_TYPES.array: {
        return PropTypes.array.isRequired;
      }
      case SERIALIZEABLE_PROP_TYPES.arrayOrNil: {
        return PropTypes.array;
      }
      case SERIALIZEABLE_PROP_TYPES.arrayOrNull: {
        return PropTypes.array.isRequiredOrNull;
      }

      default: {
        throw new Error('DevErr: invalid template value - should already be validated..');
      }
    }
  });

  const validator = validateProps(propTypesInput);
  return validator;
};

module.exports = {
  SERIALIZEABLE_PROP_TYPES,
  generateValidator,
};
